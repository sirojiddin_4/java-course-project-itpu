package edu.itpu.forjava_course_work.Extension.display;

import com.opencsv.exceptions.CsvValidationException;
import edu.itpu.forjava_course_work.Extension.controller.ProductControllerExtensionlmpl;
import edu.itpu.forjava_course_work.Extension.dao.ProductDaoExtensionImp;
import edu.itpu.forjava_course_work.Extension.service.ProductServiceExtensionlmp;
import edu.itpu.forjava_course_work.entity.Product;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;

public class ShowExtension {
    public static void main() throws IOException, CsvValidationException {

        final String ADMIN_USERNAME = "admin";
        final String ADMIN_PASSWORD = "password";
        System.out.println("""
                     Project name: Course Project. Clothes and footwear warehouse
                     Developer: sirojiddin mixritdinov, sirojiddin_mixritdinov@student.itpu.uz
                     Creation date: April 6th, 2023
                     Version: v2
                """);

        while (true) {
            ProductDaoExtensionImp productDao = new ProductDaoExtensionImp();
            ProductServiceExtensionlmp productService = new ProductServiceExtensionlmp(productDao);
            ProductControllerExtensionlmpl clothesControllerIml = new ProductControllerExtensionlmpl(productService);
            System.out.println("""
                    command(in number):
                    1.Admin
                    2.User
                    """);
            Scanner scanner = new Scanner(System.in);
            String commandRole = scanner.nextLine();
            if (commandRole.equals("1")) {
                Scanner scanner1 = new Scanner(System.in);
                System.out.print("Enter username: ");
                String username = scanner1.nextLine();
                System.out.print("Enter password: ");
                String password = scanner1.nextLine();

                if (username.equals(ADMIN_USERNAME) && password.equals(ADMIN_PASSWORD)) {
                    System.out.println("Admin logged in successfully.");
                } else {
                    System.out.println("Invalid credentials.");
                }

            } else if (commandRole.equals("2")) {

            }

            System.out.println("""
                    command(in number):
                    1.Search Product
                    2.View Product
                    3.Add product
                    4.Delete product
                    5.Add category
                    6.Exit
                    """);

            String command = scanner.nextLine();

            if (command.equals("1")) {
                System.out.println("""
                        1.By type
                        2.By brand
                        3.By season
                        """);
                Scanner sc = new Scanner(System.in);
                int searchCommand = sc.nextInt();
                sc = new Scanner(System.in);
                String typeName;

                if (searchCommand == 1) {
                    System.out.println("Please enter a value:\n [Clothes, Footwear]");
                    typeName = sc.nextLine();
                    List<Product> productBySearch = clothesControllerIml.getProductBySearch(typeName);
                    System.out.println(productBySearch.toString());
                } else if (searchCommand == 2) {
                    System.out.println("Please enter a value:\n [Dior,Adidas,Gucci,Lion,H&H,Nike,Puma]");
                    typeName = sc.nextLine();
                    List<Product> prductsBySearchByBrand = clothesControllerIml.getProductsBySearchByBrand(typeName);
                    System.out.println(prductsBySearchByBrand);

                } else if (searchCommand == 3) {
                    System.out.println("Please enter a value:\n [spring,summer]");
                    typeName = sc.nextLine();
                    List<Product> productsBySearchBySeason = clothesControllerIml.getProductsBySearchBySeason(typeName);
                    System.out.println(productsBySearchBySeason);
                } else {
                    System.err.println("Not exist command entered");
                }
            } else if (command.equals("2")) {
                List<Product> all = clothesControllerIml.getAll();
                System.out.println(all);

            } else if (command.equals("3")) {
               String res = clothesControllerIml.addProduct();
                System.out.println(res.equals("success")?"success":"failed");

            }

            else if (command.equals("4")) {
                System.out.println("Enter the Id:");
                int inputId = scanner.nextInt();
                productDao.removeService(inputId);
                System.out.println("succesfully deleted");

            }

            else if (command.equals("5")) {
                System.out.println("I am developing this feature");

            }

            else if (command.equals("6")) {
                System.out.println("Exit command entered");
                break;
            } else {
                System.err.println("You chose not exist command.Please try again");
            }
        }
    }
}
