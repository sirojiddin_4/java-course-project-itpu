package edu.itpu.forjava_course_work.Extension.controller;

import edu.itpu.forjava_course_work.Extension.dao.ProductDaoExtensionImp;
import edu.itpu.forjava_course_work.Extension.service.ProductServiceExtension;
import edu.itpu.forjava_course_work.Extension.service.ProductServiceExtensionlmp;
import edu.itpu.forjava_course_work.entity.Product;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;

public class ProductControllerExtensionlmpl implements ProductControllerExtension {
    private final ProductServiceExtension clothesService;

    public ProductControllerExtensionlmpl(ProductServiceExtensionlmp clothesService) {
        this.clothesService = clothesService;
    }

    public List<Product> getProductBySearch(String searchElement) throws IOException {
        return clothesService.getProductBySearch(searchElement);
    }

    @Override
    public List<Product> getProductsBySearchByBrand(String searchElementByBrand) throws IOException {
        return clothesService.getProductsBySearchByBrand(searchElementByBrand);


    }

    @Override
    public List<Product> getProductsBySearchBySeason(String searchElementBySeason) throws IOException {
        return clothesService.getProductsBySearchBySeason(searchElementBySeason);
    }

    @Override
    public List<Product> getAll() throws IOException {
        return clothesService.getAll();
    }

    @Override
    public String addProduct() {
        ProductDaoExtensionImp productDao = new ProductDaoExtensionImp();

        // Get the list of existing products to find the maximum ID
        List<Product> existingProducts;
        try {
            existingProducts = productDao.getproductsList();
        } catch (IOException e) {
            throw new RuntimeException("Error reading existing products", e);
        }

        // Find the maximum ID
        int maxId = existingProducts.stream()
                .mapToInt(Product::getId)
                .max()
                .orElse(0);

        // Increment the ID for the new product
        int newProductId = maxId + 1;
        Product product = new Product();
        product.setId(newProductId);
        Scanner addscan = new Scanner(System.in);
        String temp;

        do {
            System.out.print("Product name ");
            addscan.reset();
            temp = addscan.nextLine();
            if (temp.equals("")) {
                System.out.println("No data");
            }
        } while (temp.equals(""));
        product.setName(temp);

        do {
            System.out.print("Product type ");
            addscan.reset();
            temp = addscan.nextLine();
            if (temp.equals("")) {
                System.out.println("No data");
            }
        } while (temp.equals(""));
        product.setType(temp);

        boolean isNumber = false;
        do {
            System.out.print("Product price: ");
            addscan.reset();
            try {
                temp = addscan.nextLine();
                Double.parseDouble(temp);
                isNumber = true;
                System.out.println();
            } catch (NumberFormatException error) {
                System.out.println("Must be double");
                isNumber = false;
            }
            if (temp.equals("")) {
                System.out.println("No data");
            }
        } while (temp.equals("") && !isNumber);
        product.setPrice(Double.parseDouble(temp));

        do {
            System.out.print("Product quantity: ");
            addscan.reset();
            try {
                temp = addscan.nextLine();
                Integer.parseInt(temp);
                isNumber = true;
                System.out.println();
            } catch (NumberFormatException error) {
                System.out.println("Must be number");
                isNumber = false;
            }
            if (temp.equals("")) {
                System.out.println("No data");
            }
        } while (temp.equals("") && !isNumber);
        product.setQuantity(Integer.parseInt(temp));

        do {
            System.out.print("Product brand : ");
            addscan.reset();
            try {
                temp = addscan.nextLine();
                Integer.parseInt(temp);
                isNumber = true;
                System.out.println();
            } catch (NumberFormatException error) {
                System.out.println("Must be string");

            }
            if (temp.equals("")) {
                System.out.println("No data");
            }
        } while (temp.equals("") && !isNumber);
        product.setBrand(temp);

        do {
            System.out.print("Product season: ");
            addscan.reset();
            try {
                temp = addscan.nextLine();
                String.valueOf(temp);
                isNumber = true;
                System.out.println();
            } catch (NumberFormatException error) {
                System.out.println("Must be string");
            }
            if (temp.equals("")) {
                System.out.println("No data");
            }
        } while (temp.equals("") && !isNumber);
        product.setSeason(temp);
        try {
            productDao.add(product);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return "Product added successfully";

    }
}
