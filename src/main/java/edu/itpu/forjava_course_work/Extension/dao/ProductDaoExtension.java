package edu.itpu.forjava_course_work.Extension.dao;

import java.io.IOException;
import java.util.List;

public interface ProductDaoExtension {
    List<String[]> readMethod() throws IOException;
    List getproductsList() throws IOException;
}