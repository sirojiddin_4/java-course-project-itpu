package edu.itpu.forjava_course_work.Extension.dao;

import com.opencsv.CSVWriter;
import com.opencsv.exceptions.CsvValidationException;
import edu.itpu.forjava_course_work.entity.Product;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ProductDaoExtensionImp implements ProductDaoExtension {
    private static final String CSV_FILE_PATH = "src/main/resources/clothesData.csv";

    @Override
    public List<String[]> readMethod() throws IOException {
        FileReader f = new FileReader(CSV_FILE_PATH);
        List<String[]> rows;
        try (BufferedReader b = new BufferedReader(f)) {
            String line = "";
            rows = new ArrayList<String[]>();
            while ((line = b.readLine()) != null) {
                String[] product = line.split(",");
                rows.add(product);
            }
        }
        return rows;
    }

    @Override
    public List<Product> getproductsList() throws IOException {
        List<String[]> strings = readMethod();
        List<Product> productList = new ArrayList<>();
        for (int i = 0; i < strings.size(); i++) {
            String[] strings1 = strings.get(i);
            Product product = new Product(
                    Integer.valueOf(strings1[0]),
                    strings1[1],
                    strings1[2],
                    Double.valueOf(strings1[3]),
                    Integer.parseInt(strings1[4]),
                    strings1[5],
                    strings1[6]);
            productList.add(product);
        }
        return productList;
    }

    public void add(Product product) throws IOException {
        CSVWriter writer = null;
        try {
            writer = new CSVWriter(new FileWriter(CSV_FILE_PATH, true));
            String[] record = {
                    String.valueOf(product.getId()),
                    product.getName(),
                    product.getType(),
                    String.valueOf(product.getPrice()),
                    String.valueOf(product.getQuantity()),
                    product.getBrand(),
                    product.getSeason()
            };
            writer.writeNext(record);
            System.out.println("Data saved succesfully!");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                writer.close();
            }
        }
    }

    public void removeService(int id) throws CsvValidationException, IOException {
        List<Product> productList = new ArrayList<>();
        if (id <= 0 || id >= productList.size()){
            System.out.println("No id found");
            return;
        }
        for (int i=0; i < productList.size(); i++){
            if (id - 1 == i){
                productList.remove(i);
                break;
            }
        }
        update(productList);
        System.out.println("Succesfully removed");
    }

    public boolean update(List<Product> productList) throws IOException {
        CSVWriter writer = null;
        try {
            writer = new CSVWriter(new FileWriter(CSV_FILE_PATH));
            for (int i = 0; i < productList.size(); i++){
                String[]record = {
                        productList.get(i).getName(),
                        productList.get(i).getType(),
                        productList.get(i).getBrand(),
                        productList.get(i).getPrice().toString(),
                        String.valueOf(productList.get(i).getQuantity())
                };
                writer.writeNext(record);
            }
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                writer.close();
            }
        }
        return false;
    }
}
