package edu.itpu.forjava_course_work.Extension;

import com.opencsv.exceptions.CsvValidationException;
import edu.itpu.forjava_course_work.Extension.display.ShowExtension;
import edu.itpu.forjava_course_work.display.Show;

import java.io.IOException;

public class Main  {
    public static void main(String[] args) {
        try {
            ShowExtension.main();
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (CsvValidationException e) {
            throw new RuntimeException(e);
        }
    }
}
