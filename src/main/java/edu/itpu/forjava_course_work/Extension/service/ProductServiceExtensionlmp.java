package edu.itpu.forjava_course_work.Extension.service;

import edu.itpu.forjava_course_work.Extension.dao.ProductDaoExtensionImp;
import edu.itpu.forjava_course_work.entity.Product;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ProductServiceExtensionlmp implements ProductServiceExtension {
    private final ProductDaoExtensionImp productDao;

    public ProductServiceExtensionlmp(ProductDaoExtensionImp productDao) {
        this.productDao = productDao;
    }

    @Override
    public List<Product> getProductBySearch(String searchElementByType) throws IOException {
        List<Product> productList = productDao.getproductsList();
        List<Product> searchByType = new ArrayList<>();
        for (int i = 0; i < productList.size(); i++) {
            Product product = productList.get(i);
            if (product.getType().equalsIgnoreCase(searchElementByType)) {
                searchByType.add(product);
            }
        }
        if (!(searchByType.size() == 0)) {
            return searchByType;

        }
        System.out.println("There is no match up");
        return new ArrayList<>();
    }

    @Override
    public List<Product> getProductsBySearchByBrand(String searchElementByVBrand) throws IOException {
        List<Product> productList = productDao.getproductsList();
        List<Product> searchByType = new ArrayList<>();
        for (Product product : productList) {
            if (product.getBrand().equalsIgnoreCase(searchElementByVBrand)) {
                searchByType.add(product);
            }
        }
        if (!(searchByType.size() == 0)) {
            return searchByType;

        }
        System.out.println("There is no match up");
        return new ArrayList<>();
    }

    @Override
    public List<Product> getProductsBySearchBySeason(String searchElementBySeason) throws IOException {
        List<Product> productList = productDao.getproductsList();
        List<Product> searchByType = new ArrayList<>();
        for (Product product : productList) {
            if (product.getSeason().equalsIgnoreCase(searchElementBySeason)) {
                searchByType.add(product);
            }
        }
        if (!(searchByType.size() == 0)) {
            return searchByType;

        }
        System.out.println("There is no match up");
        return new ArrayList<>();
    }

    @Override
    public List<Product> getAll() throws IOException {
        return productDao.getproductsList();
    }

private Product addConsoleWarehouse(Product product) {
        Scanner addscan = new Scanner(System.in);
        String temp;

        do {
            System.out.print("Medication name: ");
            addscan.reset();
            temp = addscan.nextLine();
            if (temp.equals("")){
                System.out.println("No data");
            }
        } while (temp.equals(""));
        product.setBrand(temp);

        do {
            System.out.print("Medication type: ");
            addscan.reset();
            temp = addscan.nextLine();
            if (temp.equals("")){
                System.out.println("No data");
            }
        } while (temp.equals(""));
        product.setName(temp);

        boolean isNumber = false;
        do {
            System.out.print("Medication price: ");
            addscan.reset();
            try{
                temp = addscan.nextLine();
                Integer.parseInt(temp);
                isNumber = true;
                System.out.println();
            } catch (NumberFormatException error){
                System.out.println("Must be number");
                isNumber = false;
            }
            if (temp.equals("")){
                System.out.println("No data");
            }
        } while (temp.equals("") && !isNumber);
        product.setQuantity(Integer.parseInt(temp));

        do {
            System.out.print("Medication count: ");
            addscan.reset();
            try{
                temp = addscan.nextLine();
                Integer.parseInt(temp);
                isNumber = true;
                System.out.println();
            } catch (NumberFormatException error){
                System.out.println("Must be number");
                isNumber = false;
            }
            if (temp.equals("")){
                System.out.println("No data");
            }
        } while (temp.equals("") && !isNumber);
        product.setSeason(temp);
        return product;
    }
}
