package edu.itpu.forjava_course_work.entity;

import java.util.Objects;

public class Product {

    private Integer id;
    private String name;
    private String type;
    private Double price;
    private int quantity;
    private String brand;
    private String season;

    public Product() {

    }

    public Product(Integer id, String name, String type, Double price, int quantity, String brand, String season) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.price = price;
        this.quantity = quantity;
        this.brand = brand;
        this.season = season;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    @Override
    public String toString() {
        return displayToString();
//        return "Product{" +
//                "id=" + id +
//                ", name='" + name + '\'' +
//                ", type='" + type + '\'' +
//                ", price=" + price +
//                ", quantity=" + quantity +
//                ", brand='" + brand + '\'' +
//                ", season='" + season + '\'' +
//                "}\n";
    }

    public String  displayToString(){
        return "Product[id=+" +id+", name="+name+", type="+type+", price="+price+", quantity="+", brand="+brand+", season="+season+"\n";

    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return quantity == product.quantity && Objects.equals(id, product.id) && Objects.equals(name, product.name) && Objects.equals(type, product.type) && Objects.equals(price, product.price) && Objects.equals(brand, product.brand) && Objects.equals(season, product.season);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, type, price, quantity, brand, season);
    }

}
