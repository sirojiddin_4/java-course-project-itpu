package edu.itpu.forjava_course_work.display;

import edu.itpu.forjava_course_work.controller.ProductControllerlmpl;
import edu.itpu.forjava_course_work.dao.ProductDaoImpl;
import edu.itpu.forjava_course_work.entity.Product;
import edu.itpu.forjava_course_work.service.ProductServicelmpl;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;

public class Show {
    public static void main() throws IOException {
        System.out.println("""
                     Project name: Course Project. Clothes and footwear warehouse
                     Developer: sirojiddin mixritdinov, sirojiddin_mixritdinov@student.itpu.uz
                     Creation date: April 6th, 2023
                     Version: v1
                """);

        while (true){
            ProductDaoImpl productDao= new ProductDaoImpl();
            ProductServicelmpl productService=new ProductServicelmpl(productDao);
            ProductControllerlmpl clothesControllerIml =new ProductControllerlmpl(productService);
            System.out.println("""
                command(in number):
                1.Search Product
                2.View Product
                3.Exit
                """);
            Scanner scanner =new Scanner(System.in);
            String command = scanner.nextLine();

            if (command.equals("1")) {
                System.out.println("""
                        1.By type
                        2.By brand
                        3.By season
                        """);
                Scanner sc = new Scanner(System.in);
                int searchCommand = sc.nextInt();
                sc = new Scanner(System.in);
                String typeName ;

                if (searchCommand==1){
                    System.out.println("Please enter a value:\n [Clothes, Footwear]");
                    typeName = sc.nextLine();
                    List<Product> productBySearch = clothesControllerIml.getProductBySearch(typeName);
                    System.out.println(productBySearch.toString());
                }
                else if (searchCommand == 2) {
                    System.out.println("Please enter a value:\n [Dior,Adidas,Gucci,Lion,H&H,Nike,Puma]");
                    typeName = sc.nextLine();
                    List<Product> prductsBySearchByBrand = clothesControllerIml.getProductsBySearchByBrand(typeName);
                    System.out.println(prductsBySearchByBrand);

                } else if (searchCommand == 3) {
                    System.out.println("Please enter a value:\n [spring,summer]");
                    typeName = sc.nextLine();
                    List<Product> productsBySearchBySeason = clothesControllerIml.getProductsBySearchBySeason(typeName);
                    System.out.println(productsBySearchBySeason);
                }
                else {
                    System.err.println("Not exist command entered");
                }
            }
            else if (command.equals("2")) {
                List<Product> all = clothesControllerIml.getAll();
                System.out.println(all);

            }
            else if (command.equals("3")) {
                System.out.println("Exit command entered");
                break;
            }
            else {
                System.err.println("You chose not exist command.Please try again");
            }
        }
    }
}
