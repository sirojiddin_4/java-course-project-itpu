package edu.itpu.forjava_course_work.controller;

import edu.itpu.forjava_course_work.entity.Product;
import edu.itpu.forjava_course_work.service.ProductService;
import edu.itpu.forjava_course_work.service.ProductServicelmpl;

import java.io.IOException;
import java.util.List;

public class ProductControllerlmpl implements ProductController {
    private  final ProductService clothesService;
    public ProductControllerlmpl(ProductServicelmpl clothesService) {
        this.clothesService = clothesService;
    }

    public List<Product> getProductBySearch(String searchElement) throws IOException, IOException {
        return clothesService.getProductBySearch(searchElement);
    }

    @Override
    public List<Product> getProductsBySearchByBrand(String searchElementByVBrand) throws IOException {
        return clothesService.getProductsBySearchByBrand(searchElementByVBrand);


    }

    @Override
    public List<Product> getProductsBySearchBySeason(String searchElementBySeason) throws IOException {
        return clothesService.getProductsBySearchBySeason(searchElementBySeason);
    }

    @Override
    public List<Product> getAll() throws IOException {
        return clothesService.getAll();
    }
}
