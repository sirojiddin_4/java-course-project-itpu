package edu.itpu.forjava_course_work.service;

import edu.itpu.forjava_course_work.dao.ProductDaoImpl;
import edu.itpu.forjava_course_work.entity.Product;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ProductServicelmpl implements ProductService {
    private final ProductDaoImpl productDao;
    public ProductServicelmpl(ProductDaoImpl productDao) {
        this.productDao = productDao;
    }

    @Override
    public List<Product> getProductBySearch(String searchElementByType) throws IOException {
        List<Product> productList = productDao.getproductsList();
        List<Product> searchByType = new ArrayList<>();
        for (int i = 0; i < productList.size(); i++) {
            Product product = productList.get(i);
            if (product.getType().equalsIgnoreCase(searchElementByType)) {
                searchByType.add(product);
            }
        }
        if (!(searchByType.size() == 0)) {
            return searchByType;

        }
        System.out.println("There is no match up");
        return new ArrayList<>();
    }

    @Override
    public List<Product> getProductsBySearchByBrand(String searchElementByVBrand) throws IOException {
        List<Product> productList = productDao.getproductsList();
        List<Product> searchByType = new ArrayList<>();
        for (Product product : productList) {
            if (product.getBrand().equalsIgnoreCase(searchElementByVBrand)) {
                searchByType.add(product);
            }
        }
        if (!(searchByType.size() == 0)) {
            return searchByType;

        }
        System.out.println("There is no match up");
        return new ArrayList<>();
    }

    @Override
    public List<Product> getProductsBySearchBySeason(String searchElementBySeason) throws IOException {
        List<Product> productList = productDao.getproductsList();
        List<Product> searchByType = new ArrayList<>();
        for (Product product : productList) {
            if (product.getSeason().equalsIgnoreCase(searchElementBySeason)) {
                searchByType.add(product);
            }
        }
        if (!(searchByType.size() == 0)) {
            return searchByType;

        }
        System.out.println("There is no match up");
        return new ArrayList<>();
    }

    @Override
    public List<Product> getAll() throws IOException {
        return productDao.getproductsList();
    }
}
