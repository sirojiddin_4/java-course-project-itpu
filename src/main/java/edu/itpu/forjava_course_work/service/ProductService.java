package edu.itpu.forjava_course_work.service;

import edu.itpu.forjava_course_work.entity.Product;

import java.io.IOException;
import java.util.List;

public interface ProductService {
        List<Product> getProductBySearch(String searchElement) throws IOException;
        List<Product> getProductsBySearchByBrand(String searchElementByVBrand) throws IOException;
        List<Product> getProductsBySearchBySeason(String searchElementBySeason) throws IOException;
        List<Product> getAll() throws IOException;
}
