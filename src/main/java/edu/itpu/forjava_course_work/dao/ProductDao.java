package edu.itpu.forjava_course_work.dao;

import edu.itpu.forjava_course_work.entity.Product;

import java.io.IOException;
import java.util.List;

public interface ProductDao {
    List<String[]> readMethod() throws IOException;
    List<Product> getproductsList() throws IOException;

}