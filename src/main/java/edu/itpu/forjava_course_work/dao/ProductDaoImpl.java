package edu.itpu.forjava_course_work.dao;

import edu.itpu.forjava_course_work.entity.Product;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ProductDaoImpl implements ProductDao {
    @Override
    public List<String[]> readMethod() throws IOException {
        FileReader f = new FileReader("src/main/resources/clothesData.csv");
        List<String[]> rows;
        try (BufferedReader b = new BufferedReader(f)) {
            String line = "";
            rows = new ArrayList<String[]>();
            while ((line = b.readLine()) != null) {
                String[] product = line.split(",");
                rows.add(product);
            }
        }
        return rows;
    }

    @Override
    public List<Product> getproductsList() throws IOException {
        List<String[]> strings = readMethod();
        List<Product> productList = new ArrayList<>();
        for (int i = 0; i < strings.size(); i++) {
            String[] strings1 = strings.get(i);
            Product product = new Product(
                    Integer.valueOf(strings1[0]),
                    strings1[1],
                    strings1[2],
                    Double.valueOf(strings1[3]),
                    Integer.parseInt(strings1[4]),
                    strings1[5],
                    strings1[6]
            );
            productList.add(product);
        }
        return productList;
    }
}
