package edu.itpu.forjava_course_work;

import edu.itpu.forjava_course_work.display.Show;

import java.io.IOException;

public class Main  {
    public static void main(String[] args) {
        try {
            Show.main();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
